vcl 4.0;
#

import std;

include "hit-miss.vcl";

backend default {
    .host = "nginx";
    .port = "8081";
}

sub vcl_deliver {
    # Display hit/miss info
    if (obj.hits > 0) {
        set resp.http.V-Cache = "HIT - " + obj.hits;
    }
    else {
        set resp.http.V-Cache = "MISS - " + obj.hits;
    }
}