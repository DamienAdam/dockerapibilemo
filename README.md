# Container for API BileMo project, 

OpenClassrooms Backend/Symfony Developer path, project #7

- install docker
- clone on computer
- cd to folder
- (OPTIONAL clone API BileMo project into container folder) https://gitlab.com/DamienAdam/apibilemo.git
- docker-compose build
- docker-compose up
